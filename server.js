// setup ============================================================
var express = require('express');
var app     = express();
var mongoose= require('mongoose');


// configuration ====================================================
mongoose.connect('mongodb://node:node@novus.modulusmongo.net:27017/uvypaV7y');

app.configure(function() {
    app.use(express.static(__dirname + '/public'));
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    });


// define models ====================================================
var Todo = mongoose.model('Todo', {
    text: String
});


// listen (start app with node server.js) ===========================
var port = 8080;
app.listen(port);
console.log("App listening on " + port);


// routes ===========================================================
// api --------------------------------------------------------------
app.get('/api/todos', function(req, res) {
    Todo.find(function(err, todos) {
        if(err) {
            res.send(err);
        }
        res.json(todos);
    });
});

app.post('/api/todos', function(req, res) {
    Todo.create({
        text: req.body.text,
        done: false
    }, function(err, todo) {
        if(err) {
            res.send(err);
        }
        Todo.find(function(err, todos) {
            if(err) {
                res.send(err);
            }
            res.json(todos);
        });
    });
});

app.delete('/api/todos/:todo_id', function(req, res) {
        Todo.remove({
            _id : req.params.todo_id
        }, function(err, todo) {
            if (err)
                res.send(err);

            // get and return all the todos after you create another
            Todo.find(function(err, todos) {
                if (err)
                    res.send(err)
                res.json(todos);
        });
    });
});

// application ------------------------------------------------------
app.get('/', function(req, res) {
    res.sendfile('./public/index.html');
});